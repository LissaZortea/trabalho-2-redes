# Trabalho 2: Integração de habilidades - 2022/1
**Disciplina: Redes de Computadores**

**Curso: Engenharia de Computação**

**Laryssa Rayane Zortea/2037874:**


## Tarefa 1:  Sub-Redes
| Sub- Rede |             IPv6 - Sub-Rede            |  IPv4 - Sub-Rede  |  IPv4 - Máscara   | IPv4 - Broadcast  |    
|:---------:|:--------------------------------------:|:-----------------:|:-----------------:|:-----------------:|
| Matriz    | 2001:DB8:ACAD:4A00::/64 | 200.200.74.0   | 255.255.255.192 | 200.200.74.63  |
| Filial 1  | 2001:DB8:ACAD:4A01::/64 | 200.200.74.64  | 255.255.255.224 | 200.200.74.95  |
| Filial 2  | 2001:DB8:ACAD:4A02::/64 | 200.200.74.96  | 255.255.255.224 | 200.200.74.127 |
| Filial 3  | 2001:DB8:ACAD:4A03::/64 | 200.200.74.128 | 255.255.255.224 | 200.200.74.159 |
| Filial 4  | 2001:DB8:ACAD:4A04::/64 | 200.200.74.160 | 255.255.255.224 | 200.200.74.192 |
| Filial 5  | 2001:DB8:ACAD:4A05::/64 | 200.200.74.192 | 255.255.255.224 | 200.200.74.223 |
| pb-vit    | 2001:DB8:ACAD:4AFF::0:0/112 | 200.200.74.224 | 255.255.255.252 | 200.200.74.227 |
| vit-fb    | 2001:DB8:ACAD:4AFF::1:0/112 | 200.200.74.228 | 255.255.255.252 | 200.200.74.231 |
| fb-ita    | 2001:DB8:ACAD:4AFF::2:0/112 | 200.200.74.232 | 255.255.255.252 | 200.200.74.235 |
| ita-pb    | 2001:DB8:ACAD:4AFF::3:0/112 | 200.200.74.236 | 255.255.255.252 | 200.200.74.239 |
| cv-ita    | 2001:DB8:ACAD:4AFF::4:0/112  | 200.200.74.240 | 255.255.255.252 | 200.200.74.243 |


## Tarefa 2: Endereçamento de Dispositivos
| Dispositivo           | Interface | IPv4 | IPv4 - Máscara | IPv4 - Gateway | IPv6/Prefixo | IPv6 - Gateway |
|-----------------------|-----------|------|----------------|----------------|--------------|----------------|
| PC1 | NIC    | 200.200.74.3/26   | 255.255.255.192 | | 2001:DB8:ACAD:4A00::3/64 | |
| PC2 | NIC    | 200.200.74.4/26   | 255.255.255.192 | | 2001:DB8:ACAD:4A00::4/64 | |
| PC3 | NIC    | 200.200.74.67/27  | 255.255.255.224 | | 2001:DB8:ACAD:4A01::3/64 | |
| PC4 | NIC    | 200.200.74.68/27  | 255.255.255.224 | | 2001:DB8:ACAD:4A01::4/64 | |
| PC5 | NIC    | 200.200.74.99/27  | 255.255.255.224 | | 2001:DB8:ACAD:4A02::3/64 | |
| PC6 | NIC    | 200.200.74.100/27 | 255.255.255.224 | | 2001:DB8:ACAD:4A02::4/64 | |
| Switch-Matriz  | SVI    | 200.200.74.2/26  | 255.255.255.192 | | 2001:DB8:ACAD:4A00::2/64 | |
| Switch-Filial1 | SVI    | 200.200.74.66/27 | 255.255.255.224 | | 2001:DB8:ACAD:4A01::2/64 | |
| Switch-Filial2 | SVI    | 200.200.74.98/27 | 255.255.255.224 | | 2001:DB8:ACAD:4A02::2/64 | |
| Roteador-Pato Branco  | Fa0/0   | 200.200.74.1/30   | 255.255.255.192 | | 2001:DB8:ACAD:4A00::1/64    | FE80::1 |
| Roteador-Pato Branco  | Se0/0/0 | 200.200.74.225/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::0:1/112 | FE80::0:1|
| Roteador-Pato Branco  | Se0/0/1 | 200.200.74.238/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::3:2/112 | FE80::3:2|
| Roteador-Fco. Beltrão | Fa0/0   | 200.200.74.65/30  | 255.255.255.224 | | 2001:DB8:ACAD:4A01::1/64    | FE80::1 |
| Roteador-Fco. Beltrão | Se0/0/0 | 200.200.74.233/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::2:1/112 | FE80::2:1|
| Roteador-Fco. Beltrão | Se0/0/1 | 200.200.74.230/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::1:2/112 | FE80::1:2|
| Roteador-Vitorino     | Se0/0/0 | 200.200.74.229/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::1:1/112 | FE80::1:1|
| Roteador-Vitorino     | Se0/0/1 | 200.200.74.226/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::0:2/112 | FE80::0:2|
| Roteador-Itapejara    | Se0/0/0 | 200.200.74.237/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::3:1/112 | FE80::3:1|
| Roteador-Itapejara    | Se0/0/1 | 200.200.74.234/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::2:2/112 | FE80::2:2|
| Roteador-Itapejara    | Fa0/1   | 200.200.74.241/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::4:1/112 | FE80::4:1|
| Roteador-Coronel      | Fa0/0   | 200.200.74.97/30  | 255.255.255.224 | | 2001:DB8:ACAD:4A02::1/64    | FE80::1 |
| Roteador-Coronel      | Se0/0/1 |      |                |                |              |               |
| Roteador-Coronel      | Fa0/1   | 200.200.74.242/30 | 255.255.255.252 | | 2001:DB8:ACAD:4AFF::4:2/112 | FE80::4:2|

## Tarefa 3: Tabela de Roteamento
### Roteador Pato Branco
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.74.64  | 255.255.255.224 | 200.200.74.226 | Se 0/0/0 |
| 200.200.74.224 | 255.255.255.252 | 200.200.74.226 | Se 0/0/0 |
| 200.200.74.232 | 255.255.255.252 | 200.200.74.226 | Se 0/0/0 |
| 200.200.74.240 | 255.255.255.252 | 200.200.74.226 | Se 0/0/0 |
| 200.200.74.96  | 255.255.255.224 | 200.200.74.226 | Se 0/0/0 |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
| 2001:DB8:ACAD:4A01::/64     | 2001:DB8:ACAD:4AFF::0:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::0:0/112 | 2001:DB8:ACAD:4AFF::0:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::2:0/112 | 2001:DB8:ACAD:4AFF::0:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::4:0/112 | 2001:DB8:ACAD:4AFF::0:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A02::/64     | 2001:DB8:ACAD:4AFF::0:2 | Se 0/0/0 |

### Roteador Francisco Beltrão
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.74.0   | 255.255.255.192 | 200.200.74.234 | Se 0/0/0 |
| 200.200.74.224 | 255.255.255.252 | 200.200.74.234 | Se 0/0/0 |
| 200.200.74.236 | 255.255.255.252 | 200.200.74.234 | Se 0/0/0 |
| 200.200.74.240 | 255.255.255.252 | 200.200.74.234 | Se 0/0/0 |
| 200.200.74.96  | 255.255.255.224 | 200.200.74.234 | Se 0/0/0 |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
| 2001:DB8:ACAD:4A00::/64     | 2001:DB8:ACAD:4AFF::2:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::0:0/112 | 2001:DB8:ACAD:4AFF::2:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::3:0/112 | 2001:DB8:ACAD:4AFF::2:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::4:0/112 | 2001:DB8:ACAD:4AFF::2:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A02::/64     | 2001:DB8:ACAD:4AFF::2:2 | Se 0/0/0 |

### Roteador Vitorino
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.74.0   | 255.255.255.192 | 200.200.74.230 | Se 0/0/0 |
| 200.200.74.64  | 255.255.255.224 | 200.200.74.230 | Se 0/0/0 |
| 200.200.74.232 | 255.255.255.252 | 200.200.74.230 | Se 0/0/0 |
| 200.200.74.236 | 255.255.255.252 | 200.200.74.230 | Se 0/0/0 |
| 200.200.74.96  | 255.255.255.224 | 200.200.74.230 | Se 0/0/0 |
| 200.200.74.240 | 255.255.255.252 | 200.200.74.230 | Se 0/0/0 |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
| 2001:DB8:ACAD:4A00::/64     | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A01::/64     | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::2:0/112 | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::3:0/112 | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A02::/64     | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::4:0/112  | 2001:DB8:ACAD:4AFF::1:2 | Se 0/0/0 |

### Roteador Itapejara D'Oeste
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.74.0   | 255.255.255.192 | 200.200.74.238 | Se 0/0/0 |
| 200.200.74.64  | 255.255.255.224 | 200.200.74.238 | Se 0/0/0 |
| 200.200.74.224 | 255.255.255.252 | 200.200.74.238 | Se 0/0/0 |
| 200.200.74.228 | 255.255.255.252 | 200.200.74.238 | Se 0/0/0 |
| 200.200.74.96  | 255.255.255.224 | 200.200.74.238 | Se 0/0/0 |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
| 2001:DB8:ACAD:4A00::/64     | 2001:DB8:ACAD:4AFF::3:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A01::/64     | 2001:DB8:ACAD:4AFF::3:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::0:0/112 | 2001:DB8:ACAD:4AFF::3:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4AFF::1:0/112 | 2001:DB8:ACAD:4AFF::3:2 | Se 0/0/0 |
| 2001:DB8:ACAD:4A02::/64     | 2001:DB8:ACAD:4AFF::3:2 | Se 0/0/0 | 

### Roteador Coronel Vivida
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
| 200.200.74.0   | 255.255.255.192 | 200.200.74.241 | Fa 0/1 |
| 200.200.74.64  | 255.255.255.224 | 200.200.74.241 | Fa 0/1 |
| 200.200.74.224 | 255.255.255.252 | 200.200.74.241 | Fa 0/1 |
| 200.200.74.228 | 255.255.255.252 | 200.200.74.241 | Fa 0/1 |
| 200.200.74.232 | 255.255.255.252 | 200.200.74.241 | Fa 0/1 |
| 200.200.74.236 | 255.255.255.252 | 200.200.74.241 | Fa 0/1 |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
| 2001:DB8:ACAD:4A00::/64     | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |
| 2001:DB8:ACAD:4A01::/64     | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |
| 2001:DB8:ACAD:4AFF::0:0/112 | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |
| 2001:DB8:ACAD:4AFF::1:0/112 | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |
| 2001:DB8:ACAD:4AFF::2:0/112 | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |
| 2001:DB8:ACAD:4AFF::3:0/112 | 2001:DB8:ACAD:4AFF::4:2 | Fa 0/1 |

## Topologia - Packet Tracer
- [ ] ![Trabalho2-Topologia-NomeAluno](trabalho2-topologia-NomeAluno.pkt)


## Arquivos de Configuração dos Dispositivos Intermediários (roteadores e switches)
- [ ] ![Roteador Pato Branco](r-pb-nnn.pkt)
- [ ] ![Roteador Francisco Beltrão](r-fb-nnn.pkt)
- [ ] ![Roteador Vitorino](r-vit-nnn.pkt)
- [ ] ![Roteador Itapejara D'Oeste](r-ita-nnn.pkt)
- [ ] ![Roteador Coronel Vivida](r-cv-nnn.pkt)
- [ ] ![Switch Pato Branco](sw-pb-nnn.pkt)
- [ ] ![Switch Francisco Beltrão](sw-fb-nnn.pkt)
- [ ] ![Switch Coronel Vivida](sw-cv-nnn.pkt)


